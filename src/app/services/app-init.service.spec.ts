import { TestBed } from '@angular/core/testing';

import { AppInitService } from './app-init.service';
import { GeevService } from '../_modules/_swagger-services-module';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';

describe('AppInitService', () => {
    class GeevServiceMock {
        getProductsList() {
            return of({});
        }
    }

    class StoreMock {
        dispatch() {}
        select() {
            return of({});
        }
    }
    beforeEach(() =>
        TestBed.configureTestingModule({
            declarations: [],
            providers: [
                { provide: GeevService, useClass: GeevServiceMock },
                { provide: Store, useClass: StoreMock }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        })
    );

    it('should be created', () => {
        const service: AppInitService = TestBed.get(AppInitService);
        expect(service).toBeTruthy();
    });
});
