import { createReducer, on } from '@ngrx/store';

import { ProductsAds } from '../../_modules/_swagger-services-module';
import { addProductsData } from '../actions/products.actions';

export interface State {
    productsList: Array<ProductsAds>;
}

export const initialState: State = {
    productsList: []
};

const _productsReducer = createReducer(
    initialState,
    on(addProductsData, (state, productsToAdd) => {
        const myProducts = { ...state };

        myProducts.productsList = [
            ...myProducts.productsList,
            ...productsToAdd.products
        ];

        return myProducts;
    })
);

// tslint:disable-next-line: only-arrow-functions
export function productsReducer(state, action) {
    return _productsReducer(state, action);
}
