import { createAction, props } from '@ngrx/store';

import { Products, ProductsAds } from '../../_modules/_swagger-services-module';

export const addProductsData = createAction(
    '[Initialization] Add Products Data',
    props<{ products: Array<ProductsAds> }>()
);