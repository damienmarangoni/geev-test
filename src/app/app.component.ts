// tslint:disable-next-line: max-line-length
import {
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
    title = 'geev';

    @ViewChild('app', { static: false }) appRef: ElementRef;

    constructor() {}

    ngOnInit() {}

    ngOnDestroy() {}
}
