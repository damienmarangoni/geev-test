import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { PRODUCTS_PER_PAGE } from '../../../../shared/config';
import { addProductsData } from '../../../../store/actions/products.actions';
import { GeevService, ProductsAds } from '../../../_swagger-services-module';

@Component({
    selector: 'app-products-page',
    templateUrl: './products-page.component.html',
    styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit, OnDestroy {
    products: Array<ProductsAds>;
    fullProductsList: Array<ProductsAds>;
    lastDisplayedProductId: number;
    isLastProductLoaded = false;

    displayPageNumber = 1;
    productsToDisplayPerPage = PRODUCTS_PER_PAGE;

    requestPageNumber = 1;

    previousScrollY = 0;
    currentScrollY = 0;

    sub: Subscription;

    // Detect scroll botton reaching
    @HostListener('window:scroll', ['$event']) onWindowScroll() {
        if (
            this.isLastProductLoaded &&
            window.innerHeight + window.pageYOffset >=
                document.body.offsetHeight
        ) {
            this.fetchNextPage();
        }
    }

    constructor(
        private readonly geevService: GeevService,
        private readonly store: Store<{
            products: Array<ProductsAds>;
        }>
    ) {}

    ngOnInit() {
        // Get products list from the store
        // (first products page loaded before app init)
        this.sub = this.store
            .select('products')
            .pipe(map(products => (products as any).productsList))
            .subscribe(productsList => {
                // Test productsList validity
                Object.prototype.toString.call(productsList).slice(8, -1) ===
                'Array'
                    ? (this.fullProductsList = [...productsList])
                    : (this.fullProductsList = []);

                this.products = this.fullProductsList.slice(
                    0,
                    this.displayPageNumber * this.productsToDisplayPerPage
                );

                // Test id validity
                !!this.products[
                    this.displayPageNumber * this.productsToDisplayPerPage - 1
                ]
                    ? (this.lastDisplayedProductId = (this.products[
                          this.displayPageNumber *
                              this.productsToDisplayPerPage -
                              1
                      ] as any)._id)
                    : (this.lastDisplayedProductId = 0);
            });
    }

    lastProductLoaded() {
        this.isLastProductLoaded = true;
    }

    // Fetch next page data (display page or/and request page)
    fetchNextPage() {
        this.displayPageNumber++;
        this.products = this.fullProductsList.slice(
            0,
            this.displayPageNumber * this.productsToDisplayPerPage
        );

        // Determine last displayed product id
        if (!!this.products[this.products.length - 1]) {
            this.lastDisplayedProductId = this.products[
                this.products.length - 1
            ]._id;
        }

        // If las product displayed equals last requested product
        if (
            this.lastDisplayedProductId ===
            this.fullProductsList[this.fullProductsList.length - 1]._id
        ) {
            // Request new page of products
            this.requestPageNumber++;
            this.geevService
                .getProductsList(
                    this.requestPageNumber,
                    58,
                    '44.808219099999995,-0.6078408',
                    50000,
                    'donation',
                    false,
                    'summary',
                    'creation'
                )
                .subscribe(products => {
                    this.store.dispatch(
                        addProductsData({ products: products.ads })
                    );
                });
        }
        this.isLastProductLoaded = false;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
