import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductComponent } from './product.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { GeolocService } from 'src/app/_modules/app-products-management-services/services/geoloc.service';

describe('ProductComponent', () => {
    let component: ProductComponent;
    let fixture: ComponentFixture<ProductComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProductComponent],
            providers: [GeolocService],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductComponent);
        component = fixture.componentInstance;

        component.product = {
            title: 'toto',
            pictures: ['5dee2a293396700019ae47aa'],
            author: {
                picture: '5dc28111064c000012bc22a9'
            },
            location: {
                longitude: -0.5782194647839178,
                latitude: 44.85587785967698,
                obfuscated: true,
                radius: 1000
            }
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
